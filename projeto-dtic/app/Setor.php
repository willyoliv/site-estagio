<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setor extends Model
{
	protected $fillable = [
        'nome', 'telefone','campus','numero_setor',
    ];
    public function projects()
    {
        return $this->hasMany('App\Project');
    }
    public $rules = ['nome' => 'required|min:3|max:50|unique:setors,nome', 'telefone' => 'required|min:10','campus' => 'required|min:5|max:50','numero_setor' => 'required|integer'];
    public $rules_update = ['telefone' => 'min:10','numero_setor' => 'integer'];
    public $rules_update_telefone = ['telefone' => 'min:10'];
    public $rules_update_numero = ['numero_setor' => 'integer'];
	public $messages = ['nome.required' => 'O campo nome é de preenchimento obrigatório','nome.min' => 'O campo nome precisa ser maior', 'nome.max' => 'O campo nome precisa ser menor', 'nome.unique' => 'Setor já cadastrado','telefone.required' => 'O campo telefone é de preenchimento obrigatório','telefone.min' => 'Número inválido, muito curto','campus.required' => 'O campo Campus é de preenchimento obrigatório','campus.min' => 'O campo Campus precisa ser maior', 'nome.max' => 'O campo Campus precisa ser menor','numero_setor.required' => 'O campo com o número do setor é de preenchimento obrigatório','numero_setor.integer'=>'O campo número do setor só aceita números'];
}
