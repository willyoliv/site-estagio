<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programmer extends Model
{
    protected $fillable = [
        'nome', 'email', 'senha','matricula','cpf','telefone','tipo'
    ];
     public function projects()
    {
        return $this->belongsToMany('App\Project');
    }
    public $rules = ['nome' => 'required|min:3|max:50', 'email' => 'required|min:3|max:50|unique:programmers,email','matricula' =>'required|min:7|max:7|unique:programmers,matricula|unique:secretarias,matricula', 'cpf' => 'required|min:11|unique:programmers,cpf','telefone' => 'required|min:10'];
    public $rules_update =['telefone' => 'min:10','numero_setor' => 'integer','max:50|unique:programmers,email'];
    public $rules_update_telefone = ['telefone' => 'min:10','numero_setor' => 'integer'];
    public $rules_update_email = ['email'=>'max:50|unique:programmers,email|unique:tecnicos,email'];
	public $messages = ['nome.required' => 'O campo nome é de preenchimento obrigatório','nome.min' => 'O campo nome precisa ser maior', 'nome.max' => 'O campo nome precisa ser menor','email.min' => 'O campo email precisa ser maior','email.max' => 'O campo email precisa ser menor','email.required' => 'O campo email é de preenchimento obrigatório', 'matricula.min' => 'O campo matricula precisa ser maior','matricula.max' => 'O campo matricula precisa ser menor',
  'matricula.required' => 'O campo matricula é de preenchimento obrigatório','cpf.min' => 'CPF','cpf.required' => 'O campo cpf é de preenchimento obrigatório','cpf.unique' => 'CPF já cadastrado','email.unique' => 'Email já cadastrado', 'matricula.unique' => 'Matrícula já cadastrado','telefone.required' => 'O campo telefone é de preenchimento obrigatório','telefone.min' => 'Número inválido, muito curto'];


}
