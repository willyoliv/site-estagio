<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'nome','setor_id', 'data_inicio', 'data_termino','status','objetivos','acoes','instituicao','valor_recurso','link_hospedagem','ano','resultados',
    ];
     public function programmers()
    {
        return $this->belongsToMany('App\Programmer');
    }
    public function setor()
   {
       return $this->belongsTo('App\Setor');
   }
     public $rules = ['nome' => 'required|min:3|max:50', 'objetivos' => 'required|min:3|max:50', 'acoes' => 'required|min:5|max:200', 'valor_recurso' => 'required|numeric','instituicao' => 'required|min:3|max:50'];
     public $rules_conclusao = ['resultados' => 'min:10|max:50'];
     public $rules_nome = ['nome' => 'min:3|max:50'];
     public $rules_objetivos = ['objetivos' => 'min:3|max:50'];
     public $rules_acoes = ['acoes' => 'min:5|max:200'];
     public $rules_valor = ['valor_recurso' => 'numeric'];
     public $rules_instituicao = ['instituicao' => 'min:3|max:50'];
	   public $messages = ['resultados.min' => 'O campo resultados precisa ser maior','resultados.max' => 'O campo resultados precisa ser menor','nome.required' => 'O campo nome é de preenchimento obrigatório','nome.min' => 'O campo nome precisa ser maior','nome.max' => 'O campo nome precisa ser menor', 'objetivos.required' => 'O campo objetivo é de preenchimento obrigatório','objetivos.min' => 'O campo objetivo precisa ser maior', 'objetivos.max' => 'O campo nome precisa ser menor','link_hospedagem.required' => 'O campo link de hospedagem é de preechimento obrigatório', 'valor_recurso.required' => 'O campo valor do recurso é de preechimento obrigatório','valor_recurso.numeric' => 'O campo valor do recurso só aceita ser preenchido com números','acoes.required'=>'O campo ações é de preenchido obrigatório','acoes.min'=>'O campo de ações precisa conter mais informações','acoes.max'=>'O campo ações precisa conter menos informações'];
}
