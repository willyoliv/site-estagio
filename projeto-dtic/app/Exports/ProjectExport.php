<?php

namespace App\Exports;

use App\Project;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;


class ProjectExport implements FromView
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
   /* public function collection()
    {
        return Project::all();
    }*/
    public function view(): View
    {
        return view('planilha', [
            'projetos' => Project::all()
        ]);
    }
}
