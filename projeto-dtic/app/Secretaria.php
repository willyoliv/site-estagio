<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Secretaria extends Model
{
    protected $fillable = [
        'nome', 'matricula', 'senha','cpf','telefone',
    ];
}
