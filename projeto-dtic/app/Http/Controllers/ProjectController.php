<?php

namespace App\Http\Controllers;
use App\Project;
use App\Programmer;
use App\Setor;
use PDF;
use Illuminate\Http\Request;
use App\Exports\ProjectExport;
use Carbon\Carbon;
use \Crypt;

class ProjectController extends Controller
{
    //funções usadas pelo programador
    public function programador_projetos() {
        session_start();
        if(!isset($_SESSION['programador'])){
            return redirect('/');
        }
        $id = $_SESSION['id_programador'];
        $projetos = Programmer::find($id)->projects()->get();
        return view('list_projeto_programador',compact('projetos'));
    }
    public function cadastrar() {
        session_start();
        if(!isset($_SESSION['programador'])){
            return redirect('/');
        }
        $setores = Setor::All();
        return view('cadastro_projeto',compact('setores'));
    }
    public function cadastrar_projeto(Request $request){
        session_start();
        if(!isset($_SESSION['programador'])){
            return redirect('/');
        }
        $id = $request->get('setor_id');
        if($id == "default"){
            //$setores = Setor::All();
            $request->session()->flash('fail','Não existe setor cadastrado ou campo não preechido, cadastre o setor associado antes de cadastrar o projeto!');
            return redirect()->back();
        }
    	  $projeto = new Project();
        $dados = $request->all();
        $this->validate($request,$projeto->rules,$projeto->messages);
        $insert = $projeto->create($dados);
        if($insert){
        	$id = $_SESSION['id_programador'];
        	$programador = Programmer::find($id);
          $programador->projects()->attach($insert);
          $request->session()->flash('success','Projeto cadastrado com sucesso!');
          return redirect()->route('ListagemProjetosProgramador');
        }else{
            return redirect('cadastro_projeto');
        }
    }
    public function visualizar_dados($id){
        session_start();
        if(!isset($_SESSION['programador'])){
            return redirect('/');
        }
        //$id = $request->get('id');
        $id = \Crypt::decrypt($id);
        $projeto = Project::findOrfail($id);
        $setor = Setor::findOrfail($projeto->setor_id);
        return view('info_projeto_programador',compact('projeto','setor'));
    }
    public function visualizar_dados_update($id){
        session_start();
        if(!isset($_SESSION['programador'])){
            return redirect('/');
        }
        $id = \Crypt::decrypt($id);
        $projeto = Project::findOrfail($id);
        return view('update_projeto',compact('projeto'));
    }
    public function salvar(Request $request){
      session_start();
      if(!isset($_SESSION['programador'])){
          return redirect('/');
      }
      $projeto = Project::findOrfail($request->get('id'));
      if(!empty($request->get('nome'))){
        $validacao = $this->validate($request,$projeto->rules_nome,$projeto->messages);
        if($validacao){
          $projeto->nome = $request->get('nome');
        }else{
          return redirect()->back();
        }
      }
      if(!empty($request->get('objetivos'))){
        $validacao = $this->validate($request,$projeto->rules_objetivos,$projeto->messages);
        if($validacao){
          $projeto->objetivos = $request->get('objetivos');
        }else{
          return redirect()->back();
        }
      }
      if(!empty($request->get('instiuticao'))){
        $validacao = $this->validate($request,$projeto->rules_instituicao,$projeto->messages);
        if($validacao){
          $projeto->instituicao = $request->get('instituicao');
        }else{
          return redirect()->back();
        }
      }
      if(!empty($request->get('acoes'))){
        $validacao = $this->validate($request,$projeto->rules_acoes,$projeto->messages);
        if($validacao){
          $projeto->acoes = $request->get('acoes');
        }else{
          return redirect()->back();
        }
      }
      if(!empty($request->get('valor_recurso'))){
        $validacao = $this->validate($request,$projeto->rules_valor,$projeto->messages);
        if($validacao){
          $projeto->valor_recurso = $request->get('valor_recurso');
        }else{
          return redirect()->back();
        }
      }
      if(!empty($request->get('link_hospedagem'))){
          $projeto->link_hospedagem = $request->get('link_hospedagem');
      }
      $projeto->save();
      $id = $projeto->id;
      $id = Crypt::encrypt($id);
      $request->session()->flash('info','Projeto atualizado com sucesso!');
      return redirect("DadosProjeto/{'$id'}");
    }
    public function excluir_projeto(Request $request,$id){
        session_start();
        if(!isset($_SESSION['programador'])){
            return redirect('/');
        }
        $projeto = Project::findOrfail($id);
        $projeto->delete();
        $request->session()->flash('info','Projeto removido com sucesso!');
        return redirect()->route('ListagemProjetosProgramador');
    }
    public function alterar_status(Request $request,$id){
        session_start();
        if(!isset($_SESSION['programador'])){
            return redirect('/');
        }
        $projeto = Project::find($id);
        $projeto->status = "Em execução";
        $projeto->save();
        $request->session()->flash('info','Status atualizado!');
        return redirect()->back();
    }
    public function concluir_projeto(Request $request,$id){
        session_start();
        if(!isset($_SESSION['programador'])){
            return redirect('/');
        }
        $Project = new Project();
        $validacao = $this->validate($request,$Project->rules_conclusao,$Project->messages);
        if($validacao){
          $projeto = Project::find($id);
          $projeto->status = "Executado";
          $projeto->data_termino = Carbon::now()->toDateTimeString();
          $projeto->resultados = $request->get('resultados');
          $projeto->save();
          $request->session()->flash('info','Status atualizado!');
          return redirect()->back();
        }
        return redirect()->back();
    }
    //funções usadas pela secretária
    public function secretaria_projetos() {
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        $projetos = Project::paginate(10);
        return view('list_projeto_secretaria',compact('projetos'));
    }
    public function visualizar_dados_secretaria(Request $request){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        $id = $request->get('id');
        $projeto = Project::find($id);
        $setor = Setor::find($projeto->setor_id);
        return view('info_projeto_secretaria',compact('projeto','setor'));
    }
    public function gerarPlanilha(){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        $projectExport = new ProjectExport;
        return $projectExport->download('projetos.csv');
    }
    public function relatorio(){
      session_start();
      if(!isset($_SESSION['secretaria'])){
          return redirect('/');
      }
      $anos = Project::select('ano')->distinct()->get();
      return view('view_relatorio',compact('anos'));
    }
    public function gerar_relatorio(Request $request)
    {
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        $ano = $request->get('ano');
        if($ano == "TODOS"){
          $projetos = Project::All();
        }else{
          $projetos = Project::where('ano','=',$ano)->get();
        }
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadview('tela_pdf', compact('projetos','ano'));
        $pdf->setPaper('a4','landscape');

        return $pdf->download('projetos.pdf');
    }
    //funções usadas pelo técnico
    public function visualizar_projects(Request $request){
        session_start();
        if(!isset($_SESSION['tecnico'])){
            return redirect('/');
        }
        $id = $request->get('id');
        $projeto = Project::find($id);
        return view('info_projeto_tecnico',compact('projeto'));
    }
    public function tecnico_projetos() {
        session_start();
        if(!isset($_SESSION['tecnico'])){
            return redirect('/');
        }
        $id = $_SESSION['id_tecnico'];
        $projetos = Project::where('tecnico_id','=',$id)->get();
        return view('list_projeto_tecnico',compact('projetos'));
    }
}
