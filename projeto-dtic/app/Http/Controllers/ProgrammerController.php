<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Programmer;

class ProgrammerController extends Controller
{
    //funções para a secretária
    public function programador() {
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
    	$programadores = Programmer::All();
		return view('list_programador',compact('programadores'));
    }
    public function cadastrar() {
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        return view('cadastro_programador');
    }
    public function cadastrar_programador(Request $request){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
    	$Programmer = new Programmer();
        $dados = $request->all();
        $this->validate($request,$Programmer->rules,$Programmer->messages);
        $insert = $Programmer->create($dados);
        if($insert){
            $programadores = Programmer::All();
			//return view('list_programador',compact('programadores'));
            $request->session()->flash('success','Funcionário cadastrado com sucesso!');
            return redirect()->route('ListagemProgramadores');
        }else{
            return redirect('cadastro_programador');
        }
    }

    public function visualizar_dados($id){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        return view('info_programador',['programador' => Programmer::findOrFail($id)]);
    }
    public function excluir_programador(Request $request,$id){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        $programador = Programmer::findOrFail($id);
        $contador = $programador->projects()->count();
        if($contador>0){
            $request->session()->flash('erro','Erro ao tentar excluir, existem projetos vinculados ao funcionário!');
            return redirect()->back();
        }
        $programador->delete();
        $request->session()->flash('info','Funcionário removido com sucesso!');
        return redirect()->route('ListagemProgramadores');

    }
    //fim das funções da secretária
    //inicio das funções do programador
    public function index(){
        session_start();
        if(isset($_SESSION['programador'])){
            return view('home_programador');
        }else{
            return redirect('/');
        }

    }
    public function meu_perfil(){
      session_start();
    	if(!isset($_SESSION['programador'])){
    		return redirect('/');
    	}
      $programador = Programmer::find($_SESSION['id_programador']);
      return view('perfil_programador',compact('programador'));
    }
    public function editar_perfil(){
        session_start();
        if(!isset($_SESSION['programador'])){
            return redirect('/');
        }
        $programador = Programmer::find($_SESSION['id_programador']);
        return view('dados_programador',compact('programador'));

    }
    public function salvar_alteracoes(Request $request){
        session_start();
        if(!isset($_SESSION['programador'])){
            return redirect('/');
        }
        if(empty($request->get('telefone')) && empty($request->get('email'))){
          $request->session()->flash('info','Nenhum dado alterado!');
          return redirect('PerfilProgramador');
        }else if(!empty($request->get('telefone')) && !empty($request->get('email'))){
            $Programador = new Programmer();
            $validacao = $this->validate($request,$Programador->rules_update,$Programador->messages);
            if($validacao){
                $programador = Programmer::find($_SESSION['id_programador']);
                $programador->telefone = $request->get('telefone');
                $programador->email = $request->get('email');
                $programador->save();
                $request->session()->flash('info','Dados atualizados com sucesso!');
                return redirect()->route('PerfilProgramador');
            }else{
              return redirect()->back();
            }
        }else if(empty($request->get('telefone'))){
          $Programador = new Programmer();
          $validacao = $this->validate($request,$Programador->rules_update_email,$Programador->messages);
            if($validacao){
                $programador = Programmer::find($_SESSION['id_programador']);
                $programador->email = $request->get('email');
                $programador->save();
                $request->session()->flash('info','Dados atualizados com sucesso!');
                return redirect()->route('PerfilProgramador');
              }else{
                return redirect()->back();
              }
        }else{
            $Programador = new Programmer();
            $validacao = $this->validate($request,$Programador->rules_update_telefone,$Programador->messages);
            if($validacao){
                $programador = Programmer::find($_SESSION['id_programador']);
                $programador->telefone = $request->get('telefone');
                $programador->save();
                $request->session()->flash('info','Dados atualizados com sucesso!');
                return redirect()->route('PerfilProgramador');
            }else{
              return redirect()->back();
            }
        }
      }
      public function alterar_senha(Request $request){
        session_start();
        if(!isset($_SESSION['programador'])){
            return redirect('/');
        }
        $programador = Programmer::find($_SESSION['id_programador']);
        $programador->senha = bcrypt($request->get('senha'));
        $programador->save();
        $request->session()->flash('info','Senha atualizada com sucesso!');
        return redirect()->route('PerfilProgramador');
      }
}
