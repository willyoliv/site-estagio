<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Programmer;
use App\Secretaria;

class LoginController extends Controller
{
    public function login(Request $request){
    	$mat = $request->get('matricula');
    	$senha = $request->get('senha');
    	$programador =Programmer::where('matricula','=',$mat)->get();
    	$secretaria = Secretaria::where('matricula','=',$mat)->get();
    	$st = isset($programador[0]) ? $programador[0] : false;
    	$st2 = isset($secretaria[0]) ? $secretaria[0] : false;
		if ($st){
			$usuario = $programador[0];
            $senha_usuario = $usuario->senha;
            if (\Hash::check($senha, $senha_usuario)){
                $id = $usuario->id;
                session_start();
                $_SESSION['id_programador'] = $id;
                $_SESSION['nome_programador'] = $usuario->nome;
                $_SESSION['programador'] = 1;
                return redirect()->route('homeProgramador');
            }else{
                return redirect('/');
            }

    	}else if($st2){
    		$usuario = $secretaria[0];
            $senha_usuario = $usuario->senha;
            if (\Hash::check($senha, $senha_usuario)){
        		$id = $usuario->id;
                session_start();
                $_SESSION['id_secretaria'] = $id;
                $_SESSION['nome'] = $usuario->nome;
                $_SESSION['secretaria'] = 1;
                return redirect()->route('home');
            }else{
                return redirect('/');
            }
   		}else{
    			return redirect('/');
    	}
    }
    public function logout(){
        session_start();
        session_unset();
        session_destroy();
        return redirect('/');
    }


}
