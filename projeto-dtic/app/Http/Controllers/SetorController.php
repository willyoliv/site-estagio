<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setor;
use App\Project;

class SetorController extends Controller
{
    public function setor() {
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
    	$setores = Setor::All();
		return view('list_setor',compact('setores'));
    }
    public function cadastrar() {
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        return view('cadastro_setor');
    }
    public function cadastrar_setor(Request $request){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
    	$Setor = new Setor();
        $dados = $request->all();
        $this->validate($request,$Setor->rules,$Setor->messages);
        $insert = $Setor->create($dados);
        if($insert){
            $setores = Setor::All();
            $request->session()->flash('success','Setor cadastrado com sucesso!');
            return redirect()->route('ListagemSetores');
        }else{
            return redirect('cadastro_setor');
        }
    }

    public function visualizar_dados($id){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        return view('info_setor',['setor' => Setor::findOrFail($id)]);
    }

    public function excluir_setor(Request $request,$id){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        $contador = Project::where('setor_id','=',$id)->count();
        if($contador>0){
            $request->session()->flash('erro','Erro ao tentar excluir, existem projetos vinculados ao setor!');
            return redirect()->back();
        }
        $setor = Setor::findOrFail($id);
        $setor->delete();
        $request->session()->flash('info','Setor removido com sucesso!');
        return redirect()->route('ListagemSetores');

    }
    public function editar_setor($id){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        $setor = Setor::findOrFail($id);
        return view('dados_setor',compact('setor'));

    }
    public function salvar_alteracoes(Request $request,$id){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        if(empty($request->get('telefone')) && empty($request->get('numero_setor'))){
             $request->session()->flash('info','Nenhum dado alterado!');
            return redirect()->route('ListagemSetores');
        }else if(!empty($request->get('telefone')) && !empty($request->get('numero_setor'))){
            $Setor = new Setor();
            $validacao = $this->validate($request,$Setor->rules_update,$Setor->messages);
            if($validacao){
                $setor = Setor::findOrFail($id);
                $setor->telefone = $request->get('telefone');
                $setor->numero_setor = $request->get('numero_setor');
                $setor->save();
                $request->session()->flash('info','Setor atualizado com sucesso!');
                return redirect()->route('ListagemSetores');
            }else{
                return redirect('dados_setor');
            }
        }else if(empty($request->get('telefone'))){
            $Setor = new Setor();
            $validacao = $this->validate($request,$Setor->rules_update_numero,$Setor->messages);
            if($validacao){
                $setor = Setor::findOrFail($id);
                $setor->numero_setor = $request->get('numero_setor');
                $setor->save();
                $request->session()->flash('info','Setor atualizado com sucesso!');
                return redirect()->route('ListagemSetores');
            }else{
                return redirect('dados_setor');
            }
        }else{
            $Setor = new Setor();
            $validacao = $this->validate($request,$Setor->rules_update_telefone,$Setor->messages);
            if($validacao){
                $setor = Setor::findOrFail($id);
                $setor->telefone = $request->get('telefone');
                $setor->save();
                $request->session()->flash('info','Setor atualizado com sucesso!');
                return redirect()->route('ListagemSetores');
            }else{
                return redirect('dados_setor');
            }
        }

    }

}
