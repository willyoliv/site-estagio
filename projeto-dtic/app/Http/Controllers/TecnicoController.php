<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tecnico;
use App\setor;
use App\Project;

class TecnicoController extends Controller
{
    public function tecnico() {
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
    	 $tecnicos = Tecnico::All();
		     return view('list_tecnico',compact('tecnicos'));
    }
    public function excluir_tecnico(Request $request,$id){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        $contador = Project::where('tecnico_id','=',$id)->count();
        if($contador>0){
            $request->session()->flash('erro','Erro ao tentar excluir, existem projetos vinculados ao técnico!');
            return redirect()->back();
        }
        $tecnico = Tecnico::find($id);
        $tecnico->delete();
        $request->session()->flash('info','Técnico removido com sucesso!');
        return redirect()->route('ListagemTecnicos');
    }
    public function cadastrar() {
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
    	$setores = Setor::All();
        return view('cadastro_tecnico',compact('setores'));
    }
    public function cadastrar_tecnico(Request $request){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
    	$id = $request->get('setor_id');
        if($id == "default"){
            $setores = Setor::All();
            $request->session()->flash('fail','Não existe setor cadastrado ou campo de setor não foi preenchido, verifique antes de tentar cadastrar um técnico!');
            return redirect()->back();
        }
    	$tecnico = new Tecnico();
        $dados = $request->all();
        $this->validate($request,$tecnico->rules,$tecnico->messages);
        $insert = $tecnico->create($dados);
        if($insert){
            //$tecnicos = Tecnico::All();
            $request->session()->flash('success','Técnico cadastrado com sucesso!');
			      return redirect()->route('ListagemTecnicos');
        }else{
        	$setores = Setor::All();
            return redirect('cadastro_tecnico',compact('setores'));
        }
    }

    public function visualizar_dados($id){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
    	$tecnico = Tecnico::findOrFail($id);
    	$setor = Setor::findOrFail($tecnico->setor_id);
        return view('info_tecnico',compact('tecnico','setor'));
    }
    //funções do técnico
    public function index(){
        session_start();
        if(isset($_SESSION['tecnico'])){
            return view('home_tecnico');
        }else{
            return redirect('/');
        }
    }
    public function meu_perfil(){
      session_start();
    	if(!isset($_SESSION['tecnico'])){
    		return redirect('/');
    	}
      $tecnico = Tecnico::find($_SESSION['id_tecnico']);
      return view('perfil_tecnico',compact('tecnico'));
    }
    public function editar_perfil(){
        session_start();
        if(!isset($_SESSION['tecnico'])){
            return redirect('/');
        }
        $tecnico = Tecnico::find($_SESSION['id_tecnico']);
        return view('dados_tecnico',compact('tecnico'));

    }
    public function salvar_alteracoes(Request $request){
        session_start();
        if(!isset($_SESSION['tecnico'])){
            return redirect('/');
        }
        if(empty($request->get('telefone')) && empty($request->get('email'))){
          $request->session()->flash('info','Nenhum dado alterado!');
          return redirect('PerfilTecnico');
        }else if(!empty($request->get('telefone')) && !empty($request->get('email'))){
            $Tecnico = new Tecnico();
            $validacao = $this->validate($request,$Tecnico->rules_update,$Tecnico->messages);
            if($validacao){
                $tecnico = Tecnico::find($_SESSION['id_tecnico']);
                $tecnico->telefone = $request->get('telefone');
                $tecnico->email = $request->get('email');
                $tecnico->save();
                $request->session()->flash('info','Dados atualizados com sucesso!');
                return redirect()->route('PerfilTecnico');
            }else{
              return redirect()->back();
            }
        }else if(empty($request->get('telefone'))){
          $Tecnico = new Tecnico();
          $validacao = $this->validate($request,$Tecnico->rules_update_email,$Tecnico->messages);
            if($validacao){
                $tecnico = Tecnico::find($_SESSION['id_tecnico']);
                $tecnico->email = $request->get('email');
                $tecnico->save();
                $request->session()->flash('info','Dados atualizados com sucesso!');
                return redirect()->route('PerfilTecnico');
              }else{
                return redirect()->back();
              }
        }else{
            $Tecnico = new Tecnico();
            $validacao = $this->validate($request,$Tecnico->rules_update_telefone,$Tecnico->messages);
            if($validacao){
                $tecnico = Tecnico::find($_SESSION['id_tecnico']);
                $tecnico->telefone = $request->get('telefone');
                $tecnico->save();
                $request->session()->flash('info','Dados atualizados com sucesso!');
                return redirect()->route('PerfilTecnico');
            }else{
              return redirect()->back();
            }
        }
      }
      public function alterar_senha(Request $request){
        session_start();
        if(!isset($_SESSION['tecnico'])){
            return redirect('/');
        }
        $tecnico = Tecnico::find($_SESSION['id_tecnico']);
        $tecnico->senha = bcrypt($request->get('senha'));
        $tecnico->save();
        $request->session()->flash('info','Senha atualizada com sucesso!');
        return redirect()->route('PerfilTecnico');
      }
}
