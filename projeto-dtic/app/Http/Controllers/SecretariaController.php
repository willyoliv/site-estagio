<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Secretaria;

class SecretariaController extends Controller
{
    public function index(){
    	session_start();
    	if(isset($_SESSION['secretaria'])){
    		return view('home_secretaria');
    	}else{
    		return redirect('/');
    	}
    }
    public function meu_perfil(){
      session_start();
    	if(!isset($_SESSION['secretaria'])){
    		return redirect('/');
    	}
      $secretaria = Secretaria::find($_SESSION['id_secretaria']);
      return view('perfil_secretaria',compact('secretaria'));
    }
    public function editar_perfil(){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        $secretaria = Secretaria::find($_SESSION['id_secretaria']);
        return view('dados_secretaria',compact('secretaria'));

    }
    public function salvar_alteracoes(Request $request){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        if(empty($request->get('telefone'))){
          $request->session()->flash('info','Nenhum dado alterado!');
          return redirect('MeuPerfil');
        }else{
          $secretaria = Secretaria::find($_SESSION['id_secretaria']);
          $secretaria->telefone = $request->get('telefone');
          $secretaria->save();
          $request->session()->flash('info','Dados atualizados com sucesso!');
          return redirect()->route('MeuPerfil');
        }
      }
      public function alterar_senha(Request $request){
        session_start();
        if(!isset($_SESSION['secretaria'])){
            return redirect('/');
        }
        $secretaria = Secretaria::find($_SESSION['id_secretaria']);
        $secretaria->senha = bcrypt($request->get('senha'));
        $secretaria->save();
        $request->session()->flash('info','Senha atualizada com sucesso!');
        return redirect()->route('MeuPerfil');
      }
}
