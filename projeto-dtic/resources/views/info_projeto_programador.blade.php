<?php
  $nome = $_SESSION['nome_programador'];
?>
<!DOCTYPE html>
<head>
  <title>Dados do projeto</title>
  <meta charset="UTF-8">
  <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('jquery/jquery.slim.js')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <style>
    .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .20); }
    #btn{
      margin-left: 10px;
    }
  </style>
</head>
<body class="d-flex flex-column">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="{{url('/')}}">
        <img id="image" src="../img/logo1.png" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{url('/ListagemProjetosProgramador')}}">Projetos
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Bem vindo(a) {{ $nome }}
            </a>
            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{url('PerfilProgramador')}}">Meu Perfil</a>
              <a class="dropdown-item" href="{{url('Deslogar')}}">Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div id="page-content">
    <div class="container text-center">
      <div class="row justify-content-center">
        <div class="col-md-7">

          @if (session('info'))
          <br>
          <div class="alert alert-info alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('info') }}
          </div>
          @endif
          @if ($errors->any())
          <br>
          <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
          <div class="my-3 p-3 bg-white rounded box-shadow">
            <h4 class="border-bottom border-gray pb-2 mb-0">Dados do Projeto</h4>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Nome do Projeto: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->nome}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Instituição: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->instituicao}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Valor do recurso: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->valor_recurso}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Status: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->status}}</label>
              </div>
              <div class="form-group col-md-6">
                <b><label>Link de hospedagem: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->link_hospedagem}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Objetivos: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->objetivos}}</label>
              </div>
              <div class="form-group col-md-6">
                <b><label>Ações: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->acoes}}</label>
              </div>
              <div class="form-group col-md-6">
                <b><label>Resultados: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->resultados}}</label>
              </div>
            </div>
            <div>
              <?php if($projeto->status != "Executado"):?>
                <?php if($projeto->status == "Criado"): ?>
                     <a href="{{action('ProjectController@alterar_status', $projeto->id)}}" class="btn btn-primary">Iniciar execução</a>
                     <a href="{{route('projeto.alterar',Crypt::encrypt($projeto->id))}}"id="btn" class="btn btn-warning">Editar</a>
                     <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirm" id="btn">Excluir Projeto</button>
                 </form>
                <?php endif; ?>
                <?php if($projeto->status == "Em execução"): ?>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#finalizar">Finalizar projeto</button>
                  <a href="{{route('projeto.alterar',Crypt::encrypt($projeto->id))}}"id="btn" class="btn btn-warning">Editar</a>
                  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirm" id="btn">Excluir Projeto</button>
                  <div class="modal fade" id="finalizar" role="dialog">
                    <div class="modal-dialog modal-md">
                      <div class="modal-content">
                        <div class="modal-body">
                          <p> Resultados obtidos</p>
                          <form action="{{action('ProjectController@concluir_projeto', $projeto->id)}}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                              <textarea  class="form-control" id="resulados" name="resultados" placeholder="Resultados" maxlength="50" minlength="10"required></textarea>
                            </div>
                            <button class="btn btn-success" type="submit">Concluir</button>
                            <button type="button" data-dismiss="modal" class="btn btn-light">Cancelar</button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endif; ?>
              <?php endif; ?>
              <?php if($projeto->status == "Executado"):?>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirm" id="btn_excluir">Excluir Projeto</button>
              <?php endif; ?>
              <div class="modal fade" id="confirm" role="dialog">
                <div class="modal-dialog modal-md">
                  <div class="modal-content">
                    <div class="modal-body">
                      <p> Deseja excluir este projeto?</p>
                    </div>
                    <div class="modal-footer">
                      <a href="{{action('ProjectController@excluir_projeto', $projeto->id)}}" type="button" class="btn btn-danger" id="delete">Excluir</a>
                      <button type="button" data-dismiss="modal" class="btn btn-light">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="my-3 p-3 bg-white rounded box-shadow">
            <h4 class="border-bottom border-gray pb-2 mb-0">Dados do setor associado ao projeto</h4>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Setor: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$setor->nome}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Campus: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$setor->campus}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Telefone: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$setor->telefone}}</label>
              </div>
            </div>
          </div>

      </div>
    </div>
  </div>
  <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
    <div class="container text-center">
      <small>Copyright &copy; Controle de Projetos 2019</small>
    </div>
  </footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src="{{asset('jquery/jquery.min.js')}}"></script>
  <script src="{{asset('bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('jquery-easing/jquery.easing.min.js')}}"></script>
  <script src="{{asset('js/scrolling-nav.js')}}"></script>
</body>
</html>
