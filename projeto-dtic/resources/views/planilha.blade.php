<table>
  <thead>
    <tr>
      <th>Nome</th>
      <th>Data de inicio</th>
      <th>Data de término</th>
      <th>Status</th>
      <th>Objetivo</th>
      <th>Resultados</th>
      <th>Instituição financiadora</th>
      <th>Valor do recurso</th>
      <th>Ações executadas</th>
      <th>Link de hospedagem</th>
    </tr>
  </thead>
  <tbody>
    @foreach($projetos as $projeto)
    <tr>
      <td>{{ $projeto->nome }}</td>
      <td>{{ $projeto->data_inicio}}</td>
      <td>{{ $projeto->data_termino}}</td>
      <td>{{ $projeto->status}}</td>
      <td>{{ $projeto->objetivos}}</td>
      <td>{{ $projeto->resultados}}</td>
      <td>{{ $projeto->instituicao}}</td>
      <td>{{ $projeto->valor_recurso}}</td>
      <td>{{ $projeto->acoes}}</td>
      <td>{{ $projeto->link_hospedagem}}</td>
    </tr>
    @endforeach
  </tbody>
</table>