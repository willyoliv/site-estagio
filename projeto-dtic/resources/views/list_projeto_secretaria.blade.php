<?php
  $nome = $_SESSION['nome'];
?>
<!DOCTYPE html>
<head>
    <title>Listagem de projetos</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="jquery/jquery.slim.js" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <style>
      #link{
        margin-top: 15px;
        margin-left: 39%;
      }
      #lista{
        padding-top: 20px;
      }
    </style>
</head>
<body class="d-flex flex-column">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="{{url('/home')}}">
                <img id="image" src="../img/logo1.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{url('ListagemProjetos')}}">Projetos
                        <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{url('ListagemProgramadores')}}">Funcionários</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{url('ListagemSetores')}}">Setores</a>
                    </li>
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Bem vindo(a) {{ $nome }}
                    </a>
                      <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{url('MeuPerfil')}}">Meu Perfil</a>
                        <a class="dropdown-item" href="{{url('Relatorio')}}">Documentos</a>
                        <a class="dropdown-item" href="{{url('Deslogar')}}">Logout</a>
                      </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="page-content">
        <div class="container text-center">
        <div class="row justify-content-center">
            <div class="col-md-7">
                <div class="list-group"id="lista">
                  <h5 class="list-group-item list-group-item-action active">Projetos</h5>
                  @foreach($projetos as $projeto)
                  <form action="{{url('/DadosProjetoSecretaria')}}" method="post">
                     {{csrf_field()}}
                     <input type="hidden" name="id" value="{{$projeto->id}}">
                   <button type="submit" class="list-group-item list-group-item-action"><b>Nome: {{$projeto->nome}}</b></button>
                 </form>
                  @endforeach
                  <div id="link">
                    {{ $projetos->links() }}
                  </div>
                   <br>
                </div>
              <br>
          </div>

        </div>
        </div>
    </div>
    <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
        <div class="container text-center">
        <small>Copyright &copy; Controle de Projetos 2019</small>
        </div>
    </footer>
    <script src="jquery/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="jquery-easing/jquery.easing.min.js"></script>
    <script src="js/scrolling-nav.js"></script>
    </body>
</html>
