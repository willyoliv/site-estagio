<?php
$nome = $_SESSION['nome'];
?>
<!DOCTYPE html>
<head>
  <title>Meu Perfil</title>
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="jquery/jquery.slim.js" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script>
    $(function(){
      $("#inputSubmit").click(function(){
        var senha = $("#inputSenha").val();
        var senha2 = $("#inputSenha2").val();
        if(senha != senha2){
          event.preventDefault();
          alert("As senhas não são iguais!");
        }
      });
    });
  </script>
  <style>
    .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .20); }
    #btn_alterar_senha{
      margin-left: 20px;
    }
  </style>
</head>
<body class="d-flex flex-column">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="{{url('/home')}}">
        <img id="image" src="../img/logo1.png" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{url('ListagemProjetos')}}">Projetos
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('ListagemProgramadores')}}">Funcionários</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('ListagemSetores')}}">Setores</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Bem vindo(a) {{ $nome }}
            </a>
            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{url('MeuPerfil')}}">Meu Perfil</a>
              <a class="dropdown-item" href="{{url('Relatorio')}}">Documentos</a>
              <a class="dropdown-item" href="{{url('Deslogar')}}">Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <br>
  <div id="page-content">
    <div class="container text-center">
      <div class="row justify-content-center">
        <div class="col-md-7">
          @if (session('info'))
          <br>
          <div class="alert alert-info alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('info') }}
          </div>
          @endif
          <div class="my-3 p-3 bg-white rounded box-shadow">
            <h4 class="border-bottom border-gray pb-2 mb-0">Meus dados</h4>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Nome: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$secretaria->nome}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Matrícula: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$secretaria->matricula}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Telefone: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$secretaria->telefone}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Cpf: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$secretaria->cpf}}</label>
              </div>
            </div>
            <div class="row justify-content-center">
              <a href="{{url('EditarSecretaria')}}" class="btn btn-primary">Alterar dados</a>
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#alterar" id="btn_alterar_senha">Alterar senha</button>
              <div class="modal fade" id="alterar" role="dialog">
                <div class="modal-dialog modal-md">
                  <div class="modal-content">
                    <div class="modal-body">
                      <form action="{{url('AlterarSenhaSecretaria')}}" method="post">
                        {!! csrf_field() !!}
                        <div class="form-group">
                          <div class="form-group col-md-13">
                            <label>Nova senha</label>
                            <input type="password"class="form-control" id="inputSenha" name="senha" placeholder="Senha"  minlength="8" maxlength="11" required></input>
                          </div>
                          <div class="form-group col-md-13">
                            <label>Confirmação da nova senha</label>
                            <input type="password"class="form-control" id="inputSenha2" name="senha2" placeholder="Senha"  minlength="8" maxlength="11" required></input>
                          </div>
                        </div>
                        <button class="btn btn-success" type="submit" id="inputSubmit">Salvar</button>
                        <button type="button" data-dismiss="modal" class="btn btn-light">Cancelar</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
    <div class="container text-center">
      <small>Copyright &copy; Controle de Projetos 2019</small>
    </div>
  </footer>
  <script src="jquery/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="jquery-easing/jquery.easing.min.js"></script>
  <script src="js/scrolling-nav.js"></script>
</body>
</html>
