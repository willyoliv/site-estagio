<?php
  $nome = $_SESSION['nome'];
  $senha = bcrypt('12345678');
?>
<!DOCTYPE html>
<head>
  <title>Cadastro de Funcionários</title>
  <link rel="stylesheet" href="css/style.css">
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="jquery/jquery.slim.js" rel="stylesheet">
  <script type="text/javascript" src="js/jquery-1.2.6.pack.js"></script>
  <script type="text/javascript" src="js/jquery.maskedinput-1.1.4.pack.js"/></script>
  <script type="text/javascript">
    $(document).ready(function($){
      $("#cpf").mask("999.999.999-99");
    });
    $(document).ready(function($){
      $("#telefone").mask("(99) 9999-9999");
    });
  </script>
  <style>
   .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .20); }
  </style>
</head>
<body class="d-flex flex-column">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="{{url('/home')}}">
        <img id="image" src="../img/logo1.png" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{url('ListagemProjetos')}}">Projetos
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('ListagemProgramadores')}}">Funcionários</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('ListagemSetores')}}">Setores</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Bem vindo(a) {{ $nome }}
            </a>
            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{url('MeuPerfil')}}">Meu Perfil</a>
              <a class="dropdown-item" href="{{url('Relatorio')}}">Documentos</a>
              <a class="dropdown-item" href="{{url('Deslogar')}}">Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <br>
  <div id="page-content">
    <div class="container text-center">
      <div class="row justify-content-center">
        <div class="col-md-7">
          <div class="container">
            @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif
          </div>
          <div class="my-3 p-3 bg-white rounded box-shadow">
            <h4 class="border-bottom border-gray pb-2 mb-0">Formulario de cadastro</h4>
            <form action="{{url('CadastroProgramador')}}" method="post">
              {!! csrf_field() !!}
              <div class="form-group">
                <label>Nome</label>
                <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome">
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Matrícula</label>
                  <input type="text" class="form-control" id="matricula" name="matricula" placeholder="matrícula">
                </div>
                <div class="form-group col-md-6">
                  <label>Função</label>
                  <select class="form-control" name="tipo">
                    <option value="Técnico" selected="">Técnico</option>
                    <option value="Programador">Programador</option>
                  </select>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>CPF</label>
                  <input type="text" class="form-control" id="cpf" name="cpf" placeholder="CPF">
                </div>
                <div class="form-group col-md-6">
                  <label>Telefone</label>
                  <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone">
                  <input type="hidden" value={{$senha}} class="form-control" id="senha" name="senha">
                </div>
              </div>
              <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
              </div>
              <button type="submit" class="btn btn-primary">Cadastrar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
    <div class="container text-center">
      <small>Copyright &copy; Controle de Projetos 2019</small>
    </div>
  </footer>
  <script src="jquery/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="jquery-easing/jquery.easing.min.js"></script>
  <script src="js/scrolling-nav.js"></script>
</body>
</html>
