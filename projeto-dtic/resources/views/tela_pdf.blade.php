<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Relatório</title>
	<style>
		#image{
			float: right;
		}
		#image2{
			float: left;
		}
		#titulo{
			text-align: center;
		}
		#linha{
			background-color: #DCDCDC;
			text-align: center;
		}
	</style>
</head>
<body>
	<header>
		<img id="image2" src="{{ public_path() }}/img/brasao.png">
		<img id="image" src="{{ public_path() }}/img/logo3.png">
		<h4 id="titulo">
			GOVERNO DO ESTADO DO PIAUÍ<br>
			UNIVERSIDADE ESTADUAL DO PIAUÍ - UESPI<br>
			PRÓ-REITORIA DE PLANEJAMENTO E FINANÇAS - PROPLAN<br>
			DIRETORIA DE PLANEJAMENTO E ORÇAMENTO - DIPLAN<br>
			DIVISÃO DE DESENVOLVIMENTO INSTITUCIONAL - DDI<br>
		</h4>
		<h2 id="titulo">
			RELATÓRIO DE AÇÕES REALIZADAS <?php if($ano != "TODOS"): ?>EM {{$ano}}<?php endif; ?><br>
		</h2>
		<h3 id="titulo">
			SETOR/CAMPI/NÚCLEO: DIRETORIA DE TECNOLOGIA DA INFORMAÇÃO E COMUNICAÇÃO - DTIC
		</h3>
	</header>
	<main>
		<table border=1 cellspacing=0 cellpadding=2>
			<tr>
				<th id="linha">AÇÕES</th>
				<th id="linha">OBJETIVOS</th>
				<th id="linha">RESULTADOS ALCANÇADOS</th>
				<th colspan="2" id="linha">RECURSO FINANCEIRO</th>
				<th id="linha">PERIODO DE REALIZAÇÃO</th>
				<th id="linha">SITUAÇÃO ATUAL</th>
			</tr>
			@foreach($projetos as $projeto)
			<tr>
				<th>{{$projeto->acoes}}</th>
				<th>{{$projeto->objetivos}}</th>
				<th>{{$projeto->resultados}}</th>
				<th>{{$projeto->instituicao}}</th>
				<th><?php if($projeto->valor_recurso != 0): ?>R$ {{$projeto->valor_recurso}}<?php endif; ?></th>
				<th> {{ date( 'd/m/Y' , strtotime($projeto->data_inicio))}} <?php if($projeto->status == "Executado"): ?> Até {{ date( 'd/m/Y' , strtotime($projeto->data_termino))}}<?php endif; ?></th>
				<th>{{$projeto->status}}</th>
			</tr>
			@endforeach
		</table>
	</main>
</body>
</html>
