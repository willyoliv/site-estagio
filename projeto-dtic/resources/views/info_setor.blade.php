<?php
  $nome = $_SESSION['nome'];
?>
<!DOCTYPE html>
<head>
  <title>Dados do Setor</title>
  <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('jquery/jquery.slim.js')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <style>
    .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .20); }
    #btn_excluir{
      margin-left: 20px;
    }
  </style>
</head>
<body class="d-flex flex-column">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="{{url('/home')}}">
        <img id="image" src="../img/logo1.png" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{url('ListagemProjetos')}}">Projetos
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('ListagemProgramadores')}}">Funcionários</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('ListagemSetores')}}">Setores</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Bem vindo(a) {{ $nome }}
            </a>
            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{url('MeuPerfil')}}">Meu Perfil</a>
              <a class="dropdown-item" href="{{url('Relatorio')}}">Documentos</a>
              <a class="dropdown-item" href="{{url('Deslogar')}}">Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <br>
  <div id="page-content">
    <div class="container text-center">
      <div class="row justify-content-center">
        <div class="col-md-7">
          @if (session('erro'))
          <br>
          <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('erro') }}
          </div>
          @endif
          <div class="my-3 p-3 bg-white rounded box-shadow">
            <h4 class="border-bottom border-gray pb-2 mb-0">Dados do Setor</h4>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Nome: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$setor->nome}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Campus: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$setor->campus}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Telefone: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$setor->telefone}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Número do setor: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$setor->numero_setor}}</label>
              </div>
            </div>
            <div class="row justify-content-center">
              <a href="{{action('SetorController@editar_setor', $setor->id)}}" class="btn btn-primary">Alterar dados</a>
              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirm" id="btn_excluir">Excluir Setor</button>
              <div class="modal fade" id="confirm" role="dialog">
                <div class="modal-dialog modal-md">
                  <div class="modal-content">
                    <div class="modal-body">
                      <p> Deseja excluir este Setor?</p>
                    </div>
                    <div class="modal-footer">
                      <a href="{{action('SetorController@excluir_setor', $setor->id)}}" type="button" class="btn btn-danger" id="delete">Excluir</a>
                      <button type="button" data-dismiss="modal" class="btn btn-light">Cancelar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
    <div class="container text-center">
      <small>Copyright &copy; Controle de Projetos 2019</small>
    </div>
  </footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src="{{asset('jquery/jquery.min.js')}}"></script>
  <script src="{{asset('bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('jquery-easing/jquery.easing.min.js')}}"></script>
  <script src="{{asset('js/scrolling-nav.js')}}"></script>
</body>
</html>
