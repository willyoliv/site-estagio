<?php
$nome = $_SESSION['nome'];
?>
<!DOCTYPE html>
<head>
  <title>Dados Projeto</title>
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="jquery/jquery.slim.js" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
  <style>
    .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .20); }
  </style>
</head>
<body class="d-flex flex-column">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="{{url('/home')}}">
        <img id="image" src="../img/logo1.png" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{url('ListagemProjetos')}}">Projetos
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('ListagemProgramadores')}}">Funcionários</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('ListagemSetores')}}">Setores</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Bem vindo(a) {{ $nome }}
            </a>
            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{url('MeuPerfil')}}">Meu Perfil</a>
              <a class="dropdown-item" href="{{url('Relatorio')}}">Documentos</a>
              <a class="dropdown-item" href="{{url('Deslogar')}}">Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div id="page-content">
    <div class="container text-center">
      <div class="row justify-content-center">
        <div class="col-md-7">
          <div class="my-3 p-3 bg-white rounded box-shadow">
            <h4 class="border-bottom border-gray pb-2 mb-0">Dados do Projeto</h4>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Nome do Projeto: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->nome}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Instuição: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->instituicao}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Valor do recurso: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->valor_recurso}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Status: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->status}}</label>
              </div>
              <div class="form-group col-md-6">
                <b><label>Link de hospedagem: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->link_hospedagem}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Objetivos: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->objetivos}}</label>
              </div>
              <div class="form-group col-md-6">
                <b><label>Ações: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->acoes}}</label>
              </div>
              <div class="form-group col-md-6">
                <b><label>Resultados: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$projeto->resultados}}</label>
              </div>
            </div>
          </div>
          <div class="my-3 p-3 bg-white rounded box-shadow">
            <h4 class="border-bottom border-gray pb-2 mb-0">Dados do setor associado ao projeto</h4>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Setor: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$setor->nome}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <b><label>Campus: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$setor->campus}}</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
              <b><label>Telefone: </label></b>
              </div>
              <div class="form-group col-md-6">
                <label> {{$setor->telefone}}</label>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
    <div class="container text-center">
      <small>Copyright &copy; Controle de Projetos 2019</small>
    </div>
  </footer>
  <script src="jquery/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="jquery-easing/jquery.easing.min.js"></script>
  <script src="js/scrolling-nav.js"></script>
</body>
</html>
