<?php
use Carbon\Carbon;
$nome = $_SESSION['nome_programador'];
$data_inicio = Carbon::now()->toDateTimeString();
$ano = Carbon::now()->year;
?>
<!DOCTYPE html>
<head>
  <title>Cadastro de Projetos</title>
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="jquery/jquery.slim.js" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
  <style>
    .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .20); }
  </style>
</head>
<body class="d-flex flex-column">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="{{url('/homeProgramador')}}">
        <img id="image" src="../img/logo1.png" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{url('/ListagemProjetosProgramador')}}">Projetos
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Bem vindo(a) {{ $nome }}
            </a>
            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{url('PerfilProgramador')}}">Meu Perfil</a>
              <a class="dropdown-item" href="{{url('Deslogar')}}">Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <br>
  <div class="container">
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    @if (session('fail'))
    <div class="alert alert-danger alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       {{ session('fail') }}
    </div>
    @endif
  </div>

  <div id="page-content">
    <div class="container text-center">
      <div class="row justify-content-center">
        <div class="col-md-7">
          <div class="my-3 p-3 bg-white rounded box-shadow">
            <h4 class="border-bottom border-gray pb-2 mb-0">Formulario de cadastro</h4>
            <form action="{{url('CadastroProjeto')}}" method="post">
              {!! csrf_field() !!}
              <div class="form-group">
                <label>Nome do projeto</label>
                <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome">
              </div>
              <div class="form-group">
                <label>Objetivos</label>
                <input type="text" class="form-control" id="objetivo" name="objetivos" placeholder="Objetivo">
              </div>
              <div class="form-group">
                <label>Instituição fornecedora do recurso</label>
                <input type="text" class="form-control" id="instituicao" name="instituicao" placeholder="Nome">
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Valor do recurso</label>
                  <input type="text" class="form-control" id="matricula" name="valor_recurso" placeholder="valor">
                </div>
                <div class="form-group col-md-6">
                  <label>Link de hospedagem</label>
                  <input type="url" class="form-control" id="link" name="link_hospedagem" placeholder="link hospedagem">
                </div>
              </div>
              <div class="form-group">
                <label>Setor associado ao projeto</label>
                <select class="form-control" name="setor_id">
                  <option value="default" selected=""></option>
                  @foreach($setores as $setor)
                  <option value="{{$setor->id}}">{{$setor->nome}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>Ações a serem realizadas</label>
                <input type="text" class="form-control" id="acoes" name="acoes" placeholder="Ações">
              </div>
              <input type="hidden" value={{$ano}} class="form-control" id="data_inicio" name="ano">
              <input type="hidden" value={{$data_inicio}} class="form-control" id="data_inicio" name="data_inicio">
              <input type="hidden" value="Criado" class="form-control" id="status" name="status">
              <input type="hidden" value="" class="form-control" id="data_termino" name="data_termino">
              <button type="submit" class="btn btn-primary">Cadastrar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
    <div class="container text-center">
      <small>Copyright &copy; Controle de Projetos 2019</small>
    </div>
  </footer>
  <script src="jquery/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="jquery-easing/jquery.easing.min.js"></script>
  <script src="js/scrolling-nav.js"></script>
</body>
</html>
