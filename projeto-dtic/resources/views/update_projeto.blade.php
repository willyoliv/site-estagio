<?php
use Carbon\Carbon;
$nome = $_SESSION['nome_programador']
?>
<!DOCTYPE html>
<head>
  <title>Alterar Projeto</title>
  <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('jquery/jquery.slim.js')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">  
  <style>
    .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .20); }
  </style>
</head>
<body class="d-flex flex-column">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="{{url('/homeProgramador')}}">
        <img id="image" src="../img/logo1.png" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{url('/ListagemProjetosProgramador')}}">Projetos
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Bem vindo(a) {{ $nome }}
            </a>
            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{url('PerfilProgramador')}}">Meu Perfil</a>
              <a class="dropdown-item" href="{{url('Deslogar')}}">Logout</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <br>
  <div id="page-content">
    <div class="container text-center">
      <div class="row justify-content-center">
        <div class="col-md-7">
          @if ($errors->any())
          <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
          <div class="my-3 p-3 bg-white rounded box-shadow">
            <h4 class="border-bottom border-gray pb-2 mb-0">Formulario de atualização de dados</h4>
            <form action="{{url('SalvarAlteracoes')}}" method="post">
              {!! csrf_field() !!}
              <div class="form-group">
                <label>Nome do projeto</label>
                <input type="text" class="form-control" id="nome" name="nome" value="{{$projeto->nome}}">
              </div>
              <div class="form-group">
                <label>Objetivos</label>
                <input type="text" class="form-control" id="objetivo" name="objetivos" value="{{$projeto->objetivos}}">
              </div>
              <div class="form-group">
                <label>Instituição fornecedora do recurso</label>
                <input type="text" class="form-control" id="instituicao" name="instituicao" value="{{$projeto->instituicao}}">
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Valor do recurso</label>
                  <input type="text" class="form-control" id="matricula" name="valor_recurso" value="{{$projeto->valor_recurso}}">
                </div>
                <div class="form-group col-md-6">
                  <label>Link de hospedagem</label>
                  <input type="url" class="form-control" id="link" name="link_hospedagem" value="{{$projeto->link_hospedagem}}">
                </div>
              </div>
              <div class="form-group">
                <label>Ações a serem realizadas</label>
                <input type="text" class="form-control" id="acoes" name="acoes" value="{{$projeto->acoes}}">
              </div>
              <input type="hidden" name="id" value="{{$projeto->id}}">
              <button type="submit" class="btn btn-success">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
    <div class="container text-center">
      <small>Copyright &copy; Controle de Projetos 2019</small>
    </div>
  </footer>
  <script src="{{asset('jquery/jquery.min.js')}}"></script>
  <script src="{{asset('bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('jquery-easing/jquery.easing.min.js')}}"></script>
  <script src="{{asset('js/scrolling-nav.js')}}"></script>
</body>
</html>
