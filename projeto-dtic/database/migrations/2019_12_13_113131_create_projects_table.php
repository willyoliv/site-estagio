<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('setor_id');
            $table->foreign('setor_id')->references('id')->on('setors')->onDelete('cascade');
            $table->string('nome');
            $table->date('data_inicio');
            $table->date('data_termino')->nullable();
            $table->string('status');
            $table->string('objetivos');
            $table->string('instituicao');
            $table->double('valor_recurso');
            $table->string('link_hospedagem')->nullable();
            $table->string('ano');
            $table->string('acoes');
            $table->string('resultados')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
