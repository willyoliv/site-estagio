<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('secretarias')->insert([
            'nome' => 'Maria',
            'matricula' => '1236540',
            'senha' => bcrypt('12345678'),
            'cpf' => '123.456.789-00',
            'telefone' => '(86)3200-0000',
        ]);
    }
}
