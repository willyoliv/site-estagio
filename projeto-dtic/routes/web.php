<?php
//Rota inicial, tela de login
Route::get('/', function () {
    return view('login');
});
Route::Post('/Logar','LoginController@login');
Route::get('/Deslogar','LoginController@logout')->name('Deslogar');
//Rotas da secretaria
//Rotas da secretaria para acesso ao controller ProgrammerController
Route::get('/home','SecretariaController@index')->name('home');
Route::get('/ListagemProgramadores','ProgrammerController@programador')->name('ListagemProgramadores');
Route::get('/CadastrarProgramador','ProgrammerController@cadastrar');
Route::post('/CadastroProgramador','ProgrammerController@cadastrar_programador');
Route::get('DadosProgramador/{id}','ProgrammerController@visualizar_dados');
Route::get('ExcluirProgramador{id}','ProgrammerController@excluir_programador');
//Rotas da secretaria para acesso ao controller SetorController
Route::get('/ListagemSetores','SetorController@setor')->name('ListagemSetores');
Route::get('/CadastrarSetor','SetorController@cadastrar');
Route::post('/CadastroSetor','SetorController@cadastrar_setor');
Route::get('DadosSetor/{id}','SetorController@visualizar_dados');
Route::get('ExcluirSetor{id}','SetorController@excluir_setor');
Route::get('EditarSetor/{id}','SetorController@editar_setor');
Route::post('AlterarSetor{id}','SetorController@salvar_alteracoes');
//Rotas da secretaria para acesso ao controller ProjectController
Route::get('/ListagemProjetos','ProjectController@secretaria_projetos')->name('ListagemProjetos');
Route::post('DadosProjetoSecretaria','ProjectController@visualizar_dados_secretaria');
Route::get('Relatorio','ProjectController@relatorio');
Route::post('EmitirRelatorio','ProjectController@gerar_relatorio');
//Rota de acesso ao controller SecretariaController
Route::get('MeuPerfil','SecretariaController@meu_perfil')->name('MeuPerfil');
Route::get('EditarSecretaria','SecretariaController@editar_perfil');
Route::post('AtualizarCadastroSecretaria','SecretariaController@salvar_alteracoes');
Route::post('AlterarSenhaSecretaria','SecretariaController@alterar_senha');

//Rotas do programador
Route::get('/homeProgramador', 'ProgrammerController@index')->name('homeProgramador');
Route::get('/ListagemProjetosProgramador','ProjectController@programador_projetos')->name('ListagemProjetosProgramador');
Route::get('/CadastrarProjeto','ProjectController@cadastrar');
Route::post('/CadastroProjeto','ProjectController@cadastrar_projeto');
Route::get('DadosProjeto/{id}','ProjectController@visualizar_dados')->name('projeto.visualizar');
Route::get('AlterarDados/{id}','ProjectController@visualizar_dados_update')->name('projeto.alterar');
Route::post('SalvarAlteracoes','ProjectController@salvar');
Route::get('ExcluirProjeto{id}','ProjectController@excluir_projeto')->name('ExcluirProjeto');
Route::get('AlteraStatus{id}','ProjectController@alterar_status');
Route::post('ConcluirProjeto{id}','ProjectController@concluir_projeto')->name('ConcluirProjeto');
Route::get('PerfilProgramador','ProgrammerController@meu_perfil')->name('PerfilProgramador');
Route::get('EditarProgramador','ProgrammerController@editar_perfil')->name('EditarProgramador');
Route::post('AtualizarCadastroProgramador','ProgrammerController@salvar_alteracoes');
Route::post('AlterarSenhaProgramador','ProgrammerController@alterar_senha');

Route::get('/tabela','ProjectController@gerarPlanilha');
Route::get('/pdf','ProjectController@gerarPDF');
